#!/bin/bash
set -x
set -e

# Prep scripts
for exp_date in 092022 122022; do
  ./prepare_data_ChipIR${exp_date}.py
  ./cross_section_parser_ChipIR${exp_date}.py
  ./select_runs_ChipIR${exp_date}.py
done

./parse_due_errors_all.py