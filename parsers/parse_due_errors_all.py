#!/usr/bin/env python3
import re
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

from common import CNN_MICRO_NAME, BENCHMARKS, MEMORIES_NAMES, PARSED_DATA_PATH_VALID_092022
from common import CSV_CROSS_SECTION_PATH_122022, CSV_CROSS_SECTION_PATH_092022
from common import PARSED_DATA_PATH_092022, PARSED_DATA_PATH_122022


def check_if_valid_error(start_dt, df_cs):
    interval_df = df_cs[(df_cs["start_dt"] <= start_dt) & (df_cs["end_dt"] >= start_dt)]
    return int(interval_df.shape[0] != 0)


def draw_cycles_diff(df):
    cnn_op_names = CNN_MICRO_NAME
    del cnn_op_names["PARALLEL_VECT_AVG_MAX_POOL"], cnn_op_names["SEQUENTIAL_AVG_MAX_POOL"]
    micros = set(cnn_op_names.values())

    cnn_ops_df = df[df["Micro"].isin(micros)]
    fig, ax = plt.subplots(nrows=3, ncols=2, figsize=(10, 6))
    execution_type = ["Sequential", "Parallel"]

    for i, execution_type_i in enumerate(execution_type):
        for j, micro in enumerate(micros):
            sub_df = cnn_ops_df[(cnn_ops_df["Exec type"] == execution_type_i) & (cnn_ops_df["Micro"] == micro) &
                                (cnn_ops_df["SDC"] == 0) & (cnn_ops_df["DUE"] == 0)]
            sns.boxplot(data=sub_df, y="iteration_cycle", x="VFS Prof.", ax=ax[j, i], orient='v')
            # print(sub_df.groupby(["Micro", "Exec type", "VFS Prof."]).describe()["iteration_cycle"])
    plt.tight_layout()
    plt.show()

def main():
    # Read cross-section september 2022 ################################################################################
    cross_section_september_2022 = pd.read_csv(CSV_CROSS_SECTION_PATH_092022)
    cross_section_september_2022 = cross_section_september_2022[(cross_section_september_2022["acc_time"] > 0) &
                                                                (cross_section_september_2022["Time Beam Off"] /
                                                                 cross_section_september_2022["acc_time"] <= 0.3)]
    cross_section_september_2022["start_dt"] = pd.to_datetime(cross_section_september_2022["start_dt"])
    cross_section_september_2022["end_dt"] = pd.to_datetime(cross_section_september_2022["end_dt"])
    # Read intermediary data september 2022 ############################################################################
    df_092022 = pd.read_csv(PARSED_DATA_PATH_092022).rename(columns={"time": "start_dt"})
    df_092022["start_dt"] = pd.to_datetime(df_092022["start_dt"])
    names = {**BENCHMARKS, **CNN_MICRO_NAME, **MEMORIES_NAMES}
    df_092022["Micro"] = df_092022["benchmark"].apply(lambda x: names[x.replace("cnnop_RAD_", "")])
    df_092022["Exec type"] = df_092022["benchmark"].apply(lambda x: "Parallel" if "PARALLEL" in x else "Sequential")
    df_092022["VFS Prof."] = "optimal"
    # check if it is a valid error
    df_092022["is_valid_error"] = df_092022["start_dt"].apply(check_if_valid_error,
                                                              args=(cross_section_september_2022,))
    df_092022.to_csv(PARSED_DATA_PATH_VALID_092022, index=False)
    # Read cross-section december 2022 #################################################################################
    cross_section_december_2022 = pd.read_csv(CSV_CROSS_SECTION_PATH_122022)
    cross_section_december_2022 = cross_section_december_2022[(cross_section_december_2022["acc_time"] > 0) &
                                                              (cross_section_december_2022["Time Beam Off"] /
                                                               cross_section_december_2022["acc_time"] <= 0.3)]

    cross_section_december_2022["start_dt"] = pd.to_datetime(cross_section_december_2022["start_dt"])
    cross_section_december_2022["end_dt"] = pd.to_datetime(cross_section_december_2022["end_dt"])

    # Read intermediary data december 2022 #############################################################################
    df_122022 = pd.read_csv(PARSED_DATA_PATH_122022).rename(columns={"time": "start_dt", "vfs_profile": "VFS Prof."})
    df_122022["start_dt"] = pd.to_datetime(df_122022["start_dt"])
    df_122022["Micro"] = df_122022["benchmark"].apply(lambda x: CNN_MICRO_NAME[x.replace("cnnop_RAD_", "")])
    df_122022["Exec type"] = df_122022["benchmark"].apply(lambda x: "Parallel" if "PARALLEL" in x else "Sequential")

    # check if it is a valid error
    df_122022["is_valid_error"] = df_122022["start_dt"].apply(check_if_valid_error, args=(cross_section_december_2022,))

    # Join everything ##################################################################################################
    df = pd.concat([df_092022, df_122022]).reindex()
    df = df[df["is_valid_error"] == 1]

    # Calculate the DUE source percentages #############################################################################
    due_motivation = df[(df["DUE"] == 1) | (df["stderr"] == 1) | ~df["DUE_type"].isna()]
    due_motivation = due_motivation.groupby(["Micro", "Exec type", "VFS Prof.", "DUE_type"]).sum()
    # The sum is correct when compared to the cross-section values, verified on 01/2023
    sum_dues = due_motivation[["DUE"]].groupby(level=["Micro", "Exec type", "VFS Prof."]).sum()

    due_motivation = (due_motivation[["DUE"]] / sum_dues).unstack().fillna(0).droplevel(0, axis="columns")
    due_motivation.to_excel("/home/fernando/git_research/dac_2023/data/due_sources_tmp.xlsx")

    # Calculate the cycle impact #######################################################################################
    cycles_impact = df[['Micro', 'Exec type', 'VFS Prof.', 'DUE', 'DUE_type', 'SDC', 'SDC_type',
                        'iteration_cycle', 'stdout', 'stderr']]
    cycles_impact = cycles_impact[~cycles_impact["iteration_cycle"].isna()]


    # draw_cycles_diff(df=cycles_impact)


if __name__ == '__main__':
    main()
