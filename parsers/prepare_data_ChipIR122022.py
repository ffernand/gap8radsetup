#!/usr/bin/env python3
import glob
import os
import re

import pandas as pd

from common import LOGS_DIR_122022, PARSED_DATA_PATH_122022, MEMORIES_TO_TEST, CNN_OPS_TO_TEST
from prepare_data_ChipIR092022 import parse_errors, parse_errors_benchmarks
from prepare_data_ChipIR092022 import parse_errors_memory, parse_errors_cnn, filter_bad_errors


def main():
    data = list()
    for log_file_path in glob.glob(f"{LOGS_DIR_122022}/*GA8*.log"):
        log_m = re.match(r"(\d+)_(\d+)_(\d+)_(\d+)_(\d+)_(\d+)_GA8-(\S+)_gap8.log", os.path.basename(log_file_path))
        (year, month, day, hour, minute, seconds, benchmark) = log_m.groups()
        # start_log = datetime(year=int(year), month=int(month), day=int(day), hour=int(hour), minute=int(minute),
        #                      second=int(seconds))
        with open(log_file_path) as fp:
            log_lines = fp.readlines()
        header = log_lines[0]
        try:
            cnn_op = re.match(r".*cnnop:(\S+).*", header).group(1)
            assert cnn_op in CNN_OPS_TO_TEST
        except (AttributeError, AssertionError):
            cnn_op = None
        try:
            mem_to_test = re.match(r".*memtotest:(\S+).*", header).group(1)
            assert mem_to_test in MEMORIES_TO_TEST
        except (AttributeError, AssertionError):
            mem_to_test = None

        disable_check_seq_err_m = re.match(r".*disablecheckseqerr:(\S+) .*", header)
        disable_check_seq_err = bool(disable_check_seq_err_m.group(1)) if disable_check_seq_err_m else False

        vfs_profile = re.match(r".*vfsprof:(\S+).*", header)

        err_list = parse_errors(data_list=log_lines, log_file_path=os.path.basename(log_file_path))
        err_list = filter_bad_errors(log_errors=err_list)

        if mem_to_test:
            print(f"Parsing mem:{mem_to_test}")
            parsed_data = parse_errors_memory(data_list=err_list, mem_to_test=mem_to_test)
        elif cnn_op:
            if disable_check_seq_err is False:
                continue
            print(f"Parsing CNN OP:{cnn_op}")
            parsed_data = parse_errors_cnn(data_list=err_list, cnn_op=cnn_op)
            for ps_di in parsed_data:
                ps_di["vfs_profile"] = vfs_profile.group(1)

        else:
            print(f"Parsing {benchmark}")
            parsed_data = parse_errors_benchmarks(data_list=err_list, benchmark=benchmark, log_path=log_file_path)
        data.extend(parsed_data)

    df = pd.DataFrame(data)
    # We must readjust the runs to make sense
    # First only the iterations that have errors
    # df = df[(df["SDC"] == 1) | (df["DUE"] == 1)]
    df.to_csv(PARSED_DATA_PATH_122022, index=False)


if __name__ == '__main__':
    main()
