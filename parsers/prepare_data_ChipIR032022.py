#!/usr/bin/python3.8

import glob
import os.path
import re
from typing import List, Tuple

import pandas as pd

from common import LOGS_DIR_032022, PARSED_DATA_PATH_032022, EXEC_TIME_CSV_032022

STDOUT_PRINT_LINE = "DIFF-STDOUT-IDENTIFIED"
STDERR_PRINT_LINE = "DIFF-STDERR-IDENTIFIED"
TIMEOUT_ERROR = "TIMEOUT_ERROR"
UNICODE_ERROR = "UNICODE_ERROR"
RUNTIME_ERROR = "RUNTIME_ERROR"
ALL_STD_ERRORS = [STDOUT_PRINT_LINE, STDERR_PRINT_LINE, TIMEOUT_ERROR, UNICODE_ERROR, RUNTIME_ERROR]


def find_standard_errors(log_lines: list):
    stdout, stderr, timeout, unicode, runtime = [0] * 5
    for line in log_lines:
        if STDOUT_PRINT_LINE in line:
            stdout = 1
        if STDERR_PRINT_LINE in line:
            stderr = 1
        if TIMEOUT_ERROR in line:
            timeout = 1
        if UNICODE_ERROR in line:
            unicode = 1
        if RUNTIME_ERROR in line:
            runtime = 1
    return dict(stdout=stdout, stderr=stderr, timeout=timeout, unicode=unicode, runtime=runtime)


def parse_fir(log_lines: list):
    error_parsed = dict()
    error_parsed["SDC"] = 0

    for line in log_lines:
        if "Test failed" in line:
            error_parsed["SDC"] = 1
        if "Test success" in line:
            error_parsed["SDC"] = 0
        if "Reached illegal instruction" in line:
            error_parsed["CRASH_ILLEGAL_INST"] = 1
        if "Runner has failed" in line:
            error_parsed["CRASH_RUNNER"] = 1
        if "Memory allocation failed" in line:
            error_parsed["CRASH_MEM"] = 1
    return error_parsed


def parse_mat_mul(log_lines: list):
    error_parsed = dict()
    error_parsed["SDC"] = 0
    for line in log_lines:
        if "Error, result of different methods does not correspond!" in line:
            error_parsed["SDC"] = 1
        if "Test success" in line:
            error_parsed["SDC"] = 0
        if "Reached illegal instruction" in line:
            error_parsed["CRASH_ILLEGAL_INST"] = 1
        if "Runner has failed" in line:
            error_parsed["CRASH_RUNNER"] = 1
    return error_parsed


def parse_bilinear_resize(log_lines: list):
    error_parsed = dict()
    test_success = False
    size_ok = False
    error_parsed["SDC"] = 0
    for line in log_lines:
        if "Test failed with" in line or "Failed to load image" in line:
            error_parsed["SDC"] = 1
        if "Gray, Size: 77924 bytes" in line:
            size_ok = True
        if "Test success" in line:
            test_success = True
        if "Reached illegal instruction" in line:
            error_parsed["CRASH_ILLEGAL_INST"] = 1
        if "Runner has failed" in line:
            error_parsed["CRASH_RUNNER"] = 1
    if size_ok is False and test_success is False:
        error_parsed["SDC"] = 1

    return error_parsed


def parse_mat_add(log_lines: list):
    error_parsed = dict()
    error_parsed["SDC"] = 0
    test_success_found = False
    for line in log_lines:
        if "Test failed with" in line:
            error_parsed["SDC"] = 1
        if "Test success" in line:
            test_success_found = True
        if "Reached illegal instruction" in line:
            error_parsed["CRASH_ILLEGAL_INST"] = 1
        if "Runner has failed" in line:
            error_parsed["CRASH_RUNNER"] = 1
    if test_success_found:
        error_parsed["SDC"] = 0
    return error_parsed


def parse_mnist(log_lines: list):
    error_parsed = dict()
    error_parsed["SDC"] = 0

    for line in log_lines:
        # If there is something on STDOUT there is an SDC
        if "Recognized number" in line:
            error_parsed["SDC"] = 1
            if "Recognized number : 6" in line:
                error_parsed["CRITICAL_SDC"] = 0
            else:
                error_parsed["CRITICAL_SDC"] = 1
        if "Test failed with" in line:
            error_parsed["SDC"] = 1
        if "Reached illegal instruction" in line:
            error_parsed["CRASH_ILLEGAL_INST"] = 1
        if "Runner has failed" in line:
            error_parsed["CRASH_RUNNER"] = 1
        if "Cluster open failed" in line:
            error_parsed["CRASH_CLUSTER"] = 1

    return error_parsed


def compute_error(log_lines: list, benchmark: str, iteration_data: re.Match, log_file_name: str):
    base_dict = find_standard_errors(log_lines=log_lines)
    if benchmark == "Fir":
        error_dict = parse_fir(log_lines=log_lines)
    elif benchmark == "MatMult":
        error_dict = parse_mat_mul(log_lines=log_lines)
    elif benchmark == "MatrixAdd":
        error_dict = parse_mat_add(log_lines=log_lines)
    elif benchmark == "Mnist" or benchmark == "MnistGraph":
        error_dict = parse_mnist(log_lines=log_lines)
    elif benchmark == "BilinearResize":
        error_dict = parse_bilinear_resize(log_lines=log_lines)
    else:
        raise ValueError(f"Incorrect benchmark value {benchmark}")
    if base_dict["stdout"] == 1 and error_dict["SDC"] == 0:
        base_dict["stdout"] = 0
    error_parsed = {
        **base_dict, **error_dict,
        "benchmark": benchmark,
        "machine": "GAP8",
        "header": f"GAP8_{benchmark}",
        "time": iteration_data.group(1),
        "iteration": int(iteration_data.group(2)),
        "iteration_time": float(iteration_data.group(3)),
        "acc_time": float(iteration_data.group(5)),
        "file_path": log_file_name,
    }
    # print(iteration_data.groups())
    if all([not i for i in error_parsed.values()]):
        print(f"INCORRECT PARSING {log_lines}")
    return error_parsed


def parse_errors(log_file_path: str, benchmark: str) -> Tuple[List[dict], List[dict]]:
    iteration_pattern = r"(\d+\-\d+\-\d+ \d+:\d+:\d+) run_experiment.py INFO Iteration:(\d+) "
    iteration_pattern += r"time:(\S+)s it_errors:(\d+) acc_time:(\S+) "
    iteration_pattern += r"acc_errors:(\d+) seq_errors:(\d+) run_experiment.*"
    # error_start_pattern = r"(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+) run_experiment.py ERROR"
    # Keep an array of errors to not add sequentially errors
    # and return it in the end
    error_list = list()
    with open(log_file_path) as fp:
        log_lines = fp.readlines()

    line_i = 0
    mark_lines = dict()
    no_error_iterations_time_list = list()

    while line_i < len(log_lines):
        line = log_lines[line_i]
        err_tmp_list = list()
        # Search until next iteration line
        while re.match(iteration_pattern, line) is None and line_i < len(log_lines):
            # Skip reboot lines
            if "reboot" not in line.lower() and "HEADER" not in line.upper():
                err_tmp_list.append(line)
            line = log_lines[line_i]
            line_i += 1
        # The iteration line always exists
        iteration_data = re.match(iteration_pattern, line)
        # Nothing to process anymore
        if len(log_lines) == line_i and iteration_data is None:
            return error_list, no_error_iterations_time_list
        mark_lines[line] = dict(err_list=err_tmp_list, iteration_data=iteration_data)

        line_i += 1

    # Iterate over the groups
    iteration_lines = list(mark_lines.keys())
    iteration_lines_length = len(iteration_lines)
    average_no_error_iteration_time = 0
    for it_line in range(1, iteration_lines_length - 1):
        key_i_before = iteration_lines[it_line - 1]
        key_i_current = iteration_lines[it_line]
        key_i_after = iteration_lines[it_line + 1]
        # This means that it is a single error
        it_exec_time = float(mark_lines[key_i_current]["iteration_data"].group(3))
        if not mark_lines[key_i_current]["err_list"]:
            average_no_error_iteration_time += it_exec_time
            no_error_iterations_time_list.append({"benchmark": benchmark, "execution_time": it_exec_time})
        elif not mark_lines[key_i_before]["err_list"] and not mark_lines[key_i_after]["err_list"]:
            last_error_computed = compute_error(log_lines=mark_lines[key_i_current]["err_list"], benchmark=benchmark,
                                                iteration_data=mark_lines[key_i_current]["iteration_data"],
                                                log_file_name=log_file_path)
            error_list.append(last_error_computed)
        # Save all the exec time values

    if iteration_lines_length >= 2:
        # Iteration 0
        key_0 = iteration_lines[0]
        key_1 = iteration_lines[1]
        if not mark_lines[key_1]["err_list"] and mark_lines[key_0]["err_list"]:
            last_error_computed = compute_error(log_lines=mark_lines[key_0]["err_list"], benchmark=benchmark,
                                                iteration_data=mark_lines[key_0]["iteration_data"],
                                                log_file_name=log_file_path)
            error_list.append(last_error_computed)
        # Iteration n
        key_n_before = iteration_lines[-2]
        key_n = iteration_lines[-1]
        if not mark_lines[key_n_before]["err_list"] and mark_lines[key_n]["err_list"]:
            last_error_computed = compute_error(log_lines=mark_lines[key_n]["err_list"], benchmark=benchmark,
                                                iteration_data=mark_lines[key_n]["iteration_data"],
                                                log_file_name=log_file_path)
            error_list.append(last_error_computed)
    # if "2022_03_29_14_12_54_GA8-MatrixAdd_gap8.log" in log_file_path:
    #     print(error_list)
    # Putting the average error free execution time
    average_no_error_iteration_time /= iteration_lines_length
    for err in error_list:
        err["no_error_execution_time"] = average_no_error_iteration_time
    return error_list, no_error_iterations_time_list


def main():
    extracted_data = list()
    no_error_iterations_time = list()
    for logfile in glob.glob(f"{LOGS_DIR_032022}/*.log"):
        log_m = re.match(r"(\d+)_(\d+)_(\d+)_(\d+)_(\d+)_(\d+)_GA8-(\S+)_gap8.log", os.path.basename(logfile))
        (year, month, day, hour, minute, seconds, benchmark) = log_m.groups()
        # start_log = datetime(year=int(year), month=int(month), day=int(day), hour=int(hour), minute=int(minute),
        #                      second=int(seconds))
        log_error_list, exec_time_list = parse_errors(log_file_path=logfile, benchmark=benchmark)
        extracted_data.extend(log_error_list)
        no_error_iterations_time.extend(exec_time_list)
    df = pd.DataFrame(extracted_data)
    df.to_csv(PARSED_DATA_PATH_032022, index=False, sep=";")
    print("Saved at", PARSED_DATA_PATH_032022)
    exec_time_df = pd.DataFrame(no_error_iterations_time)

    exec_time_df.to_csv(EXEC_TIME_CSV_032022, index=False)
    print("Saved exec time at", EXEC_TIME_CSV_032022)


if __name__ == '__main__':
    main()
