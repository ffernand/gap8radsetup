#!/usr/bin/env python3
import copy
import glob
import os
import pickle
import re
from datetime import datetime
from typing import List, Tuple

import pandas as pd

from common import LOGS_DIR_092022, PARSED_DATA_PATH_092022, MEMORIES_TO_TEST, CNN_OPS_TO_TEST
from common import PARSED_DATA_MASKED_PATH_092022

STDOUT_PRINT_LINE = "DIFF-STDOUT-IDENTIFIED"
STDERR_PRINT_LINE = "DIFF-STDERR-IDENTIFIED"
TIMEOUT_ERROR = "TIMEOUT_ERROR"
UNICODE_ERROR = "UNICODE_ERROR"
RUNTIME_ERROR = "RUNTIME_ERROR"
RUNNER_HAS_FAILED = "Runner has failed"
ILLEGAL_INSTRUCTION = "Reached illegal instruction"
MEMORY_ALLOCATION_FAILED = "Memory allocation failed"
CLUSTER_OPEN_FAILED = "Cluster open failed"
ALL_STD_ERRORS = [
    STDOUT_PRINT_LINE, STDERR_PRINT_LINE, TIMEOUT_ERROR, UNICODE_ERROR, RUNTIME_ERROR, RUNNER_HAS_FAILED,
    ILLEGAL_INSTRUCTION, MEMORY_ALLOCATION_FAILED, CLUSTER_OPEN_FAILED
]


def find_standard_errors(log_lines: list):
    stdout_diff = stderr_diff = 0
    due, due_type = None, None
    for line in log_lines:
        if STDOUT_PRINT_LINE in line:
            stdout_diff = 1
        if STDERR_PRINT_LINE in line:
            stderr_diff = 1
        if TIMEOUT_ERROR in line:
            due_type = TIMEOUT_ERROR
        if UNICODE_ERROR in line:
            due_type = UNICODE_ERROR
        if RUNTIME_ERROR in line:
            due_type = RUNTIME_ERROR
        if ILLEGAL_INSTRUCTION in line:
            due_type = ILLEGAL_INSTRUCTION
        if MEMORY_ALLOCATION_FAILED in line:
            due_type = MEMORY_ALLOCATION_FAILED
        if CLUSTER_OPEN_FAILED in line:
            due_type = CLUSTER_OPEN_FAILED
    due = int(due_type is not None)
    return stdout_diff, stderr_diff, due, due_type


def parse_errors_memory(data_list: list, mem_to_test: str, log_base_path: str) -> List[dict]:
    error_list, golden_mem_val_str = list(), "AAAAAAAA"
    golden_mem_val_list = [*('1010' * len(golden_mem_val_str))]
    memory_err_list = list()
    for iteration_i_data in data_list:
        new_it_data = copy.deepcopy(iteration_i_data)
        new_it_data["benchmark"] = f"mem_{mem_to_test}"
        err_i_list = new_it_data["err_list"]
        mp_count = 0
        if err_i_list:
            stdout_diff, stderr_diff, due, due_type = find_standard_errors(log_lines=err_i_list)
            new_it_data["stdout"] = stdout_diff
            new_it_data["DUE"] = due
            new_it_data["DUE_type"] = due_type
            new_it_data["zero_to_one"] = 0
            new_it_data["one_to_zero"] = 0
            new_it_data["SDC"] = 0
            positions = list()

            iteration_check_set = set()
            data_errors = list()
            for err_i in err_i_list:
                err_m = re.match(r"i:(\d+) M\[(\d+)]=(\S+)", err_i.strip())
                if err_m:
                    print(err_m.groups())
                    new_it_data["SDC"] = 1
                    corrupted_mem_str = err_m.group(3).rjust(len(golden_mem_val_str), "a")
                    position = int(err_m.group(2))
                    positions.append(position)
                    iteration_check_set.add(int(err_m.group(1)))
                    data_errors.append((int(err_m.group(2)), err_m.group(3)))

                    # print(err_m.group(3), corrupted_mem_str, err_i)
                    # Check if the length is equal
                    assert len(golden_mem_val_str) == len(corrupted_mem_str)
                    corrupted_mem_list = [i for i in "{0:032b}".format(int(corrupted_mem_str, 16))]
                    # Check if only one bit is flipped in each error
                    diff_bit_position = [i for i, (g, f) in enumerate(zip(golden_mem_val_list, corrupted_mem_list)) if
                                         g != f]
                    assert len(diff_bit_position) == 1, f"{corrupted_mem_str} {golden_mem_val_str}"
                    diff_bit_position = diff_bit_position[0]
                    # If it passed after the asserted line, it is ok to go
                    new_it_data["zero_to_one"] += int(
                        golden_mem_val_list[diff_bit_position] == '0' and corrupted_mem_list[diff_bit_position] == '1'
                    )
                    new_it_data["one_to_zero"] += int(
                        golden_mem_val_list[diff_bit_position] == '1' and corrupted_mem_list[diff_bit_position] == '0'
                    )

                    mp_count += 1

            if data_errors:
                if len(iteration_check_set) != 1:
                    raise ValueError(f"More than one iteration per error set: {iteration_check_set}")

                memory_err_list.append({"iteration": next(iter(iteration_check_set)), "mem_type": mem_to_test,
                                        "data": data_errors})

            new_it_data["adjacent_bits"] = 0
            new_it_data["nonadjacent_bits"] = 0
            positions.sort()
            if len(positions) == 2:
                new_it_data["adjacent_bits"] += int(positions[0] == (positions[1] - 1))
                new_it_data["nonadjacent_bits"] += int(positions[0] != (positions[1] - 1))
            else:
                for i in range(1, len(positions) - 1):
                    new_it_data["adjacent_bits"] += int(positions[i - 1] == (positions[i] - 1))
                    new_it_data["nonadjacent_bits"] += int(positions[i - 1] != (positions[i] - 1))
            new_it_data["bits_affected"] = mp_count
            del new_it_data["err_list"]
            error_list.append(new_it_data)
    with open(f"/home/fernando/git_research/gap8radsetup/parsers/data/{log_base_path.replace('.log', '.data')}",
              "wb") as fp:
        pickle.dump(memory_err_list, fp)
    return error_list


def parse_errors_cnn(data_list: list, cnn_op: str) -> List[dict]:
    error_list = list()

    for iteration_i_data in data_list:
        new_it_data = copy.deepcopy(iteration_i_data)
        new_it_data["benchmark"] = f"cnnop_{cnn_op}"
        err_list = new_it_data["err_list"]

        if err_list:
            # The case where the runner died for real
            if any(["Error: no device found" in err_list_it for err_list_it in err_list]):
                raise ValueError(f"RUN DIED FOR REAL, INVESTIGATE\n{err_list}")
            stdout_diff, stderr_diff, due, due_type = find_standard_errors(log_lines=err_list)
            new_it_data["SDC"] = 0
            new_it_data["stdout"] = stdout_diff
            new_it_data["stderr"] = stderr_diff
            new_it_data["DUE"] = due
            new_it_data["DUE_type"] = due_type
            for line in err_list:
                # Search for SDC
                error_match = re.match(r"Error:\[-?\d+(?:,-?\d+)?]=(-?\d+) != (-?\d+)", line.strip())
                if stdout_diff == 1 and error_match:
                    # Gold can be equal too, then not actually an error
                    groups = list(map(int, error_match.groups()))
                    if groups[0] != groups[1]:
                        new_it_data["SDC"] = 1
                    new_it_data["DUE"] = 0

                additional_info_m = re.match(r".*RATIT:(\S+) ITS:(\S+) TIME_IT:(\S+) CYCLE[_\\x7f]+IT:(\S+) .*", line)
                if additional_info_m:
                    try:
                        new_it_data["its"] = int(additional_info_m.group(2))
                    except ValueError:
                        new_it_data["its"] = None
                    try:
                        new_it_data["iteration_cycle"] = int(additional_info_m.group(4))
                    except ValueError:
                        new_it_data["iteration_cycle"] = None
            del new_it_data["err_list"]
            error_list.append(new_it_data)
    return error_list


def parse_benchmark_log_lines(log_lines: list, benchmark: str) -> Tuple[int, str]:
    sdc, sdc_type = 0, ""
    if "Mnist" in benchmark:
        test_failed_found = False
        for line in log_lines:
            # If there is something on STDOUT there is an SDC
            m = re.match(r"ErrorIt:.* Class:(\d+).*", line)
            if m:
                sdc = 1
                sdc_type = "critical" if int(m.group(1) != '6') else "tolerable"
            if "Test failed with" in line:
                sdc = 1
                test_failed_found = True
        if test_failed_found is True and sdc_type == "":
            raise ValueError("\n".join(log_lines))
    else:
        for line in log_lines:
            m = re.match(r"Error:\[.*]=(\S+) != (\S+)", line)
            if m:
                try:
                    found, golden = int(m.group(1)), int(m.group(2))
                except ValueError:
                    try:
                        found, golden = int(m.group(1), 16), int(m.group(2), 16)
                    except ValueError:
                        found, golden = float(m.group(1)), float(m.group(2))

                if found != golden:
                    sdc = 1
    return sdc, sdc_type


def parse_errors_benchmarks(data_list: list, benchmark: str, log_path: str) -> List[dict]:
    error_list = list()
    for iteration_i_data in data_list:
        new_it_data = copy.deepcopy(iteration_i_data)
        new_it_data["benchmark"] = benchmark

        err_list = new_it_data["err_list"]
        if err_list:
            # The case where the runner died for real
            if any(["Error: no device found" in err_list_it for err_list_it in err_list]):
                continue
            stdout_diff, stderr_diff, due, due_type = find_standard_errors(log_lines=err_list)
            new_it_data["DUE"] = due
            new_it_data["DUE_type"] = due_type
            sdc, sdc_type = parse_benchmark_log_lines(log_lines=err_list, benchmark=benchmark)
            new_it_data["SDC"] = sdc
            new_it_data["SDC_type"] = sdc_type

            for err_i_list in err_list:
                additional_info_m = re.match(r".*ITS:(\d+) .*CYCLES_T1:(\d+) CYCLES_T2:(\d+) .*", err_i_list)
                if additional_info_m:
                    new_it_data["its"] = int(additional_info_m.group(1))
                    new_it_data["iteration_cycle"] = int(additional_info_m.group(3)) - int(additional_info_m.group(2))
            del new_it_data["err_list"]
            error_list.append(new_it_data)
    return error_list


def parse_errors(data_list: list, log_file_path: str) -> List[dict]:
    iteration_pattern = r"(\d+\-\d+\-\d+ \d+:\d+:\d+) run_experiment.py INFO Iteration:(\d+) time:(\S+)s " \
                        r"it_errors:(\d+) acc_time:(\S+) acc_errors:(\d+) seq_errors:(\d+) " \
                        r"cycle_str:(.*)err_cycle:(.*)run_experiment.*"

    # 25-09-22 18:44:14 run_experiment.py INFO Iteration:1 time:72.5063s it_errors:0 acc_time:145.05023050308228
    # acc_errors:0 seq_errors:0 cycle_str:None err_cycle:None run_experiment.py:367
    # error_start_pattern = r"(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+) run_experiment.py ERROR"
    # Keep an array of errors to not add sequentially errors
    # and return it in the end
    # If it is the mem benchmark
    iterations_list_data = list()
    err_list = None
    for i, line in enumerate(data_list):
        iteration_m = re.match(iteration_pattern, line)
        if iteration_m:
            cycle_str = iteration_m.group(8).strip()
            err_cycle = iteration_m.group(9).strip()
            iterations_list_data.append(dict(
                time=datetime.strptime(iteration_m.group(1), "%d-%m-%y %H:%M:%S"),
                iteration=int(iteration_m.group(2)),
                iteration_time=float(iteration_m.group(3)),
                iteration_errors=int(iteration_m.group(4)),
                acc_time=float(iteration_m.group(5)),
                seq_errors=int(iteration_m.group(7)),  # Useful to find bad errors when runner died
                err_list=err_list,
                file_path=log_file_path,
                cycle_str=None if cycle_str == "None" else cycle_str,
                err_cycle=None if err_cycle == "None" else err_cycle,
            ))
            err_list = None
        else:
            # Not an iteration line
            if "reboot" not in line.lower() and "HEADER" not in line.upper():
                if err_list is None:
                    err_list = list()
                err_list.append(line.strip())

            if "INFO Iteration" in line and "DIFF" not in line:
                raise ValueError(f"Incorrect parsing:{line}")
    return iterations_list_data


def filter_bad_errors(log_errors: List[dict]) -> List[dict]:
    new_log_errors: List[dict] = list()
    for err in log_errors:
        err_list = err['err_list']
        if err_list:
            if err['seq_errors'] > 0 and err['iteration_time'] <= 1.0:
                assert any(["Error: no device found" in err_list_it for err_list_it in err_list]), "Something wrong"
            else:
                new_log_errors.append(err)
    return new_log_errors


def extract_masked_data(data_list: List[dict], mem_to_test: str, cnn_op: str, benchmark: str) -> List[dict]:
    new_log_errors: List[dict] = list()
    for data in data_list:
        if data["err_list"] is None:
            di = data.copy()
            di["benchmark"] = benchmark
            di["mem"] = mem_to_test
            di["cnn_op"] = cnn_op
            new_log_errors.append(di)

    return new_log_errors


def main():
    not_masked_data = list()
    masked_data = list()
    for log_file_path in glob.glob(f"{LOGS_DIR_092022}/*.log"):
        log_m = re.match(r"(\d+)_(\d+)_(\d+)_(\d+)_(\d+)_(\d+)_GA8-(\S+)_gap8.log", os.path.basename(log_file_path))
        (year, month, day, hour, minute, seconds, benchmark) = log_m.groups()
        # start_log = datetime(year=int(year), month=int(month), day=int(day), hour=int(hour), minute=int(minute),
        #                      second=int(seconds))
        with open(log_file_path) as fp:
            log_lines = fp.readlines()
        header = log_lines[0]
        try:
            cnn_op = re.match(r".*cnnop:(\S+).*", header).group(1)
            assert cnn_op in CNN_OPS_TO_TEST
        except (AttributeError, AssertionError):
            cnn_op = None
        try:
            mem_to_test = re.match(r".*memtotest:(\S+).*", header).group(1)
            assert mem_to_test in MEMORIES_TO_TEST
        except (AttributeError, AssertionError):
            mem_to_test = None

        disable_check_seq_err_m = re.match(r".*disablecheckseqerr:(\S+) .*", header)
        disable_check_seq_err = bool(disable_check_seq_err_m.group(1)) if disable_check_seq_err_m else False

        err_list = parse_errors(data_list=log_lines, log_file_path=os.path.basename(log_file_path))
        masked_data.extend(
            extract_masked_data(data_list=err_list, mem_to_test=mem_to_test, cnn_op=cnn_op, benchmark=benchmark)
        )

        err_list = filter_bad_errors(log_errors=err_list)

        if mem_to_test:
            print(f"Parsing mem:{mem_to_test}")
            parsed_data = parse_errors_memory(data_list=err_list, mem_to_test=mem_to_test,
                                              log_base_path=os.path.basename(log_file_path))
        elif cnn_op:
            if disable_check_seq_err is False:
                continue
            print(f"Parsing CNN OP:{cnn_op}")
            parsed_data = parse_errors_cnn(data_list=err_list, cnn_op=cnn_op)
        else:
            print(f"Parsing {benchmark}")
            parsed_data = parse_errors_benchmarks(data_list=err_list, benchmark=benchmark, log_path=log_file_path)
        not_masked_data.extend(parsed_data)
    df = pd.DataFrame(not_masked_data)
    # We must readjust the runs to make sense
    # First only the iterations that have errors
    df.to_csv(PARSED_DATA_PATH_092022, index=False)
    df_masked = pd.DataFrame(masked_data)
    df_masked.to_csv(PARSED_DATA_MASKED_PATH_092022, index=False)


if __name__ == '__main__':
    main()
