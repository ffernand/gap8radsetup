#!/bin/bash

set -e
set -x

LOGS_DIR=/home/fernando/Dropbox/ChipIR092022_GAP8

REPO_DIR=/home/fernando/git_research/gap8radsetup
mkdir -p $REPO_DIR/data

for log_path in "${LOGS_DIR}"/*.tar.gz; do
  tar xzf "$log_path" -C $REPO_DIR/
done

#mv $REPO_DIR/data/logs/first_morning/*.log  $REPO_DIR/data/logs/
#rm -r $REPO_DIR/data/logs/first_morning

