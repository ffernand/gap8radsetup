#!/usr/bin/python3.8
import math

import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import ticker

from common import BENCHMARKS, MARKER_OPTIONS, CSV_CROSS_SECTION_PATH_032022, EXEC_TIME_CSV_032022


def calc_err_sdc(row):
    return (1.96 * row["Cross Section SDC"]) / (math.sqrt(row["SDC"]))


def calc_err_due(row):
    return (1.96 * row["Cross Section DUE"]) / (math.sqrt(row["DUE"]))


def get_fit(cross_section_database_path):
    df = pd.read_csv(cross_section_database_path)
    # Select the good runs
    df = df[
        # only the ones that have ACC time
        (df["acc_time"] > 0) &
        # Remove broken runs
        # more than 1/3
        (df["Time Beam Off"] / df["acc_time"] <= 0.3) &
        # SDC > 0 or DUE > 0, if at least one is bigger than zero
        ((df["SDC"] > 0) | (df["DUE"] > 0))
        ]
    # Remove unnecessary info
    # gap8_cs = df[["benchmark", "start_dt", "end_dt", "SDC", "DUE", "acc_time", "Flux 1h", "Time Beam Off"]].copy()
    gap8_cs = df.copy()
    # Fluency hand calc
    # gap8_cs["Fluency"] = gap8_cs["acc_time"] * gap8_cs["Flux 1h"]
    gap8_grouped = gap8_cs.groupby(["benchmark"]).sum()
    # gap8_grouped = gap8_grouped[(gap8_grouped["SDC"] > 0) & (gap8_grouped["DUE"] > 0)]
    """ Calc cross section """
    gap8_grouped["Cross Section SDC"] = (gap8_grouped["SDC"] / gap8_grouped["Fluency(Flux * $AccTime)"])
    gap8_grouped["Cross Section DUE"] = (gap8_grouped["DUE"] / gap8_grouped["Fluency(Flux * $AccTime)"])
    gap8_grouped["Err SDC"] = gap8_grouped.apply(calc_err_sdc, axis="columns")
    gap8_grouped["Err DUE"] = gap8_grouped.apply(calc_err_due, axis="columns")
    return gap8_grouped


def draw_mebf(df, execution_time):
    """
    Impact of GPUs Parallelism Management on Safety-Critical and HPC Applications Reliability
    """
    # df["FIT SDC"] = df["Cross Section SDC"] * DEFAULT_SEA_FLUX
    # df["FIT DUE"] = df["Cross Section DUE"] * DEFAULT_SEA_FLUX
    # print(df)
    # 13 is from 13e9
    full_fit = (df["Cross Section SDC"] + df["Cross Section DUE"]) * 13
    full_fit_err = (df["Err SDC"] + df["Err DUE"])
    # MTBF - Mean Time Between Failure = 1 / (cross_section * sea_flux)
    df["MTBF"] = 1.0 / full_fit
    df["Err MTBF"] = (df["MTBF"] / (df["Cross Section SDC"] + df["Cross Section DUE"])) * full_fit_err
    # MEBF - Mean Execution Between Failure = MTBF / execution_time
    # execution time must be in hours
    exec_time_hour = (execution_time / 3600)
    df["MEBF"] = df["MTBF"] / exec_time_hour
    df["Err MEBF"] = df["Err MTBF"] / exec_time_hour
    # Calc the average
    df = df.transpose()
    df["Average"] = df.mean(axis="columns")
    df = df.transpose()
    # to_paper = df[
    #     ["SDC", "DUE", "Flux 1h", "Fluency(Flux * $AccTime)", "Cross Section SDC", "Cross Section DUE", "MTBF"]]
    # to_paper["Cross Section"] = (df["Cross Section SDC"] + df["Cross Section DUE"])
    # print(to_paper)
    # exit()
    y_err = df[["Err MEBF"]].rename({"Err MEBF": "MEBF"}, axis="columns")
    ax = df["MEBF"].plot.bar(yerr=y_err["MEBF"], edgecolor="black", color="darkgrey", capsize=2)
    ax.yaxis.set_major_formatter(ticker.FormatStrFormatter("%2.e"))

    bars = ax.patches
    bars[-1].set_hatch("//")
    ax.set_ylabel("Mean Executions Between Failure (MEBF)", fontsize=12)
    ax.set_xlabel("")
    # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.tick_params(labelrotation=0, labelsize=12)
    plt.tight_layout()
    plt.savefig("/home/fernando/git_research/iolts_riscvw_2022/fig/mebf.pdf")
    plt.show()


def draw_cross_section(df):
    # Calc the average
    df = df.transpose()
    df["Average"] = df.mean(axis="columns")
    # ratio_mnist = df["Mnist"] / df["Average"]
    # ratio_fir = df["Average"] / df["Fir"]
    #
    # print(ratio_mnist)
    # print(ratio_fir)
    # exit()
    df = df.transpose()
    sdc_str = "Silent Data Corruption (SDC)"
    due_str = "Detected Unrecoverable Error (DUE)"
    df_draw = df[["Cross Section SDC", "Cross Section DUE"]].rename(
        {"Cross Section SDC": sdc_str, "Cross Section DUE": due_str}, axis="columns")
    y_err = df[["Err SDC", "Err DUE"]].rename({"Err SDC": sdc_str, "Err DUE": due_str}, axis="columns")

    # fig, ax = plt.subplots(figsize=(6.5, 4))
    ax = df_draw.plot.bar(yerr=y_err, capsize=2, edgecolor="black",
                          color=[MARKER_OPTIONS["SDC"]["color"], MARKER_OPTIONS["DUE"]["color"]])
    bars = ax.patches
    bars[-1].set_hatch("//")
    bars[-7].set_hatch("//")

    ax.set_ylabel("Cross Section", fontsize=12)
    # print(df[["Cross section SDC", "Cross-section DUE", "Err SDC", "Err DUE"]].max().max())
    ax.yaxis.set_major_formatter(ticker.FormatStrFormatter("%.e"))
    ax.set_xlabel("")
    # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.tick_params(labelrotation=0, labelsize=12)
    plt.legend(frameon=False)  # , fontsize=10.5, bbox_to_anchor=(1.1, 1.15), ncol=2, )
    plt.tight_layout()
    plt.savefig("/home/fernando/git_research/iolts_riscvw_2022/fig/cross_section.pdf")
    # plt.savefig("/home/fernando/git_research/radecs_riscvw_2022/fig/cross_section.svg")

    plt.show()


def main():
    # csv_path = "../data/parsed_ChipIR092022/parsed_data_cross_section.csv"
    df = get_fit(cross_section_database_path=CSV_CROSS_SECTION_PATH_032022)
    df = df.reindex(index=list(BENCHMARKS.keys())).reset_index()
    df["benchmark"] = df["benchmark"].apply(lambda x: BENCHMARKS[x])
    df = df.set_index("benchmark")
    # Gen the MEBF
    exec_time_df = pd.read_csv(EXEC_TIME_CSV_032022)
    exec_time_df = exec_time_df[exec_time_df["benchmark"] != "MnistGraph"]
    exec_time_df["benchmark"] = exec_time_df["benchmark"].apply(lambda x: BENCHMARKS[x])
    execution_time = exec_time_df.groupby("benchmark").mean()["execution_time"]
    draw_mebf(df=df, execution_time=execution_time)
    print(df)
    # Gen the Cross-section
    draw_cross_section(df)
    # Save to GitHub of the paper
    df[["SDC", "DUE",
        "Cross Section SDC", "Cross Section DUE", "Err SDC", "Err DUE",
        "MEBF", "Err MEBF"]].to_excel("/home/fernando/git_research/iolts_riscvw_2022/data/cross_section.xlsx")


if __name__ == '__main__':
    main()
