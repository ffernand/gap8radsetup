#!/usr/bin/env python3

import pandas as pd

from common import read_count_file, get_fluency_flux, SECONDS_1h, PARSED_DATA_PATH_032022


def generate_cross_section(row, neutron_count):
    start_dt = row["start_dt"]
    end_dt = row["end_dt"]
    acc_time = row["acc_time"]
    sdc_s = row["SDC"]
    due_s = row["DUE"]
    # distance_line = distance_data[(distance_data["board"].str.contains(machine)) &
    #                               (distance_data["start"] <= start_dt) & (start_dt <= distance_data["end"])]
    # # factor = float(distance_line["factor"])
    # distance_factor = float(distance_line["Distance attenuation"])
    # Conversion factor (as usual 4th column of counters to get n/cm2) is 4.3E+5, when you use the large beam,
    # and 6.1E+5 when you use the small beam.
    large_beam_limit = pd.to_datetime("2022-03-29 14", format='%Y-%m-%d %H')
    factor = 6.1E+5
    if start_dt <= large_beam_limit:
        factor = 4.3E+5
    r0 = 14
    x = 126
    distance_attenuation = r0 ** 2 / ((x / 100 + r0) ** 2)

    print(f"Generating cross section for {row['benchmark']}, start {start_dt} end {end_dt}")
    flux, time_beam_off = get_fluency_flux(start_dt=start_dt, end_dt=end_dt, neutron_count=neutron_count,
                                           facility_factor=factor, distance_attenuation=distance_attenuation)
    fluency = flux * acc_time
    cross_section_sdc = cross_section_due = 0
    if fluency > 0:
        cross_section_sdc = sdc_s / fluency
        cross_section_due = due_s / fluency

    row["end_dt"] = end_dt
    row["Flux 1h"] = flux
    row["Fluency(Flux * $AccTime)"] = fluency
    row["Cross Section SDC"] = cross_section_sdc
    row["Cross Section DUE"] = cross_section_due
    row["Time Beam Off"] = time_beam_off
    return row


def get_end_times(group):
    # Check if it is sorted
    assert group.equals(group.sort_values())
    end_dt_group = group.copy()
    # Find the time slots
    first = group.iloc[0]
    time_slot_for_fluency = pd.Timedelta(seconds=SECONDS_1h)
    for i in range(len(group)):
        if (group.iloc[i] - first) > time_slot_for_fluency:
            first = group.iloc[i]
        end_dt_group.iloc[i] = first + time_slot_for_fluency
    return end_dt_group


def get_end_times_bigger_than_1h(group):
    # Check if it is sorted
    assert group.equals(group.sort_values())
    end_dt_group = group.copy()
    # Find the time slots
    first = group.iloc[0]
    time_slot_for_fluency = group.iloc[-1] - first
    for i in range(len(group)):
        end_dt_group.iloc[i] = first + time_slot_for_fluency
    print(first)
    print(end_dt_group)
    return end_dt_group


def classify_due_gap8(row):
    search_dues = ["CRASH_ILLEGAL_INST", "CRASH_MEM", "CRASH_CLUSTER", "timeout", "unicode", "runtime"]
    #     lambda row: 1 if row["#end"] == 0 and row["#abort"] == 0 else 0
    if any([row[i] == 1 for i in search_dues]):
        return 1
    return 0


def main():
    neutron_count_file = "data/neutron_logs/merged_neutronlog.txt"
    # csv_file_name = "data/parsed_ChipIR032022/parsed_data.csv"
    print(f"Generating cross section for {PARSED_DATA_PATH_032022}")
    print(f"- {neutron_count_file} for neutrons")
    # -----------------------------------------------------------------------------------------------------------------
    # We need to read the neutron count files before calling get_fluency_flux
    neutron_count = read_count_file(neutron_count_file)
    csv_out_file_summary = PARSED_DATA_PATH_032022.replace(".csv", "_cross_section.csv")
    print(f"in: {PARSED_DATA_PATH_032022}")
    print(f"out: {csv_out_file_summary}")
    # -----------------------------------------------------------------------------------------------------------------
    # Read the input csv file
    input_df = pd.read_csv(PARSED_DATA_PATH_032022, delimiter=';')  # .drop("file_path", axis="columns")

    # Before continuing we need to invert the logic of abort and end
    input_df["DUE"] = input_df.apply(classify_due_gap8, axis="columns")
    # Convert time to datetime
    input_df["time"] = pd.to_datetime(input_df["time"])
    # Rename the column
    input_df = input_df.rename(columns={"time": "start_dt"}).sort_values(by=["start_dt"])
    ####################################################################################################################
    # # TO use the full acc time MAYBE INCORRECT
    # # Separate the runs that are bigger than 1h
    # runs_bigger_than_1h = input_df[input_df["acc_time"] > SECONDS_1h].copy()
    # # runs_bigger_than_1h["file_path"] = runs_bigger_than_1h["file_path"].apply(lambda x: os.path.basename(x))
    # # runs_bigger_than_1h["full_end_dt"] = runs_bigger_than_1h["start_dt"] + pd.to_timedelta(
    # #     runs_bigger_than_1h["acc_time"], unit='s')
    # runs_bigger_than_1h['end_dt'] = runs_bigger_than_1h.groupby(['benchmark'])['start_dt'].transform(
    #     get_end_times_bigger_than_1h)
    # runs_bigger_than_1h = runs_bigger_than_1h.groupby(['benchmark', 'end_dt']).agg(
    #     {'start_dt': 'first', 'SDC': 'sum', 'acc_time': 'sum', 'DUE': 'sum'}).reset_index()
    # # print(runs_bigger_than_1h)
    # # exit()
    # # Group by hours. Only 1h runs can be grouped
    # runs_1h = input_df[input_df["acc_time"] <= SECONDS_1h].copy()
    # # print(runs)
    # # runs = input_df
    # runs_1h['end_dt'] = runs_1h.groupby(['benchmark'])['start_dt'].transform(get_end_times)
    # runs_1h = runs_1h.groupby(['benchmark', 'end_dt']).agg(
    #     {'start_dt': 'first', 'SDC': 'sum', 'acc_time': 'sum', 'DUE': 'sum'}).reset_index()
    # runs = pd.concat([runs_1h, runs_bigger_than_1h]).reset_index()
    ####################################################################################################################
    # TO USE only 1h ACC TIME and bigger acc times will be placed in chunks
    runs = input_df.copy()
    runs['end_dt'] = runs.groupby(['benchmark'])['start_dt'].transform(get_end_times)
    runs = runs.groupby(['benchmark', 'end_dt']).agg(
        {'start_dt': 'first', 'SDC': 'sum', 'acc_time': 'sum', 'DUE': 'sum', "file_path": "first"}).reset_index()
    runs["original_acc_time"] = runs["acc_time"]
    runs.loc[runs["acc_time"] > SECONDS_1h, "acc_time"] = SECONDS_1h
    ####################################################################################################################
    # Apply generate_cross section function
    final_df = runs.apply(generate_cross_section, axis="columns", args=(neutron_count,))
    # Reorder before saving
    final_df = final_df[
        ['start_dt', 'end_dt', 'benchmark', 'SDC', 'DUE', 'acc_time', "original_acc_time", 'Time Beam Off', 'Flux 1h',
         'Fluency(Flux * $AccTime)', 'Cross Section SDC', 'Cross Section DUE', "file_path"]]
    print(final_df)
    print(csv_out_file_summary)
    final_df.to_csv(csv_out_file_summary, index=False, date_format="%Y-%m-%d %H:%M:%S")


#########################################################
#                    Main Thread                        #
#########################################################
if __name__ == '__main__':
    main()
