#!/usr/bin/python3.8
import math

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from common import BENCHMARKS, MARKER_OPTIONS, EXEC_SDC
from common import EXEC_TIME_CSV


def plot_exec_time_distribution(df):
    df["ratio_exec_time"] = df["iteration_time"] / df["no_error_execution_time"]
    # Remove the outliers
    df = df[~((df["benchmark"] == BENCHMARKS["Fir"]) & (df["ratio_exec_time"] > 9))]
    df = df[~((df["benchmark"] == BENCHMARKS["MatMult"]) & (df["ratio_exec_time"] > 8.2))]
    df = df[~((df["benchmark"] == BENCHMARKS["Mnist"]) & (df["ratio_exec_time"] > 10))]
    df = df[df["outcome"] != "Not an error"]

    # Remove incorrect config for bi linear
    df = df.drop(
        df[(df["benchmark"] == BENCHMARKS["BilinearResize"]) & (df["outcome"] == "Timeout") & (
                df["iteration_time"] < 50)].index)
    # Get the colors and markers for the legend
    all_markers = {k: v["marker"] for k, v in MARKER_OPTIONS.items()}
    all_colors = {k: v["color"] for k, v in MARKER_OPTIONS.items()}
    # time_outcome = df[["benchmark", "outcome", "ratio_exec_time", "no_error_execution_time", "iteration_time"]]
    # time_outcome.groupby(["benchmark", "outcome"]).describe().to_csv("/tmp/test.csv")
    df.loc[df["outcome"] == "Runtime error", "outcome"] = "SDK exception"
    # Order of the columns
    df = df.set_index("benchmark").loc[list(BENCHMARKS.values())].reset_index()

    df_no_timeout = df[df["outcome"] != "Timeout"]
    ###########################################################################################################
    # EXEC TIME ONLY
    # print(df_no_timeout[["benchmark", "iteration_time", "time", "ratio_exec_time"]].groupby("benchmark").describe())
    # sns.boxplot(x="benchmark", y="iteration_time", data=df_no_timeout, hue="outcome")

    ###########################################################################################################
    fig, ax = plt.subplots(figsize=(13, 7))
    sns.stripplot(x="benchmark", y="ratio_exec_time", data=df_no_timeout, edgecolor="black", linewidth=1, jitter=0.2,
                  palette=all_colors, hue="outcome", ax=ax, s=15,
                  hue_order=[EXEC_SDC, "Illegal instruction", "Memory error",
                             "SDK exception"])  # markers=all_markers, style="outcome",
    # ax.axhline(y=9, linewidth=260, alpha=0.5, color='black')

    # sns.scatterplot(x="benchmark", y="ratio_exec_time", data=df, edgecolor="black", s=40,
    #                 markers=all_markers, palette=all_colors, hue="outcome", style="outcome", ax=ax)
    # for points in ax.collections:
    #     vertices = points.get_offsets().data
    #     if len(vertices) > 0:
    #         vertices[:, 0] += np.random.uniform(-0.3, 0.3, vertices.shape[0])
    #         points.set_offsets(vertices)
    # xticks = ax.get_xticks()
    # ax.set_xlim(xticks[0] - 0.5, xticks[-1] + 0.5)  # the limits need to be moved to show all the jittered dots
    # sns.move_legend(ax, bbox_to_anchor=(1.01, 1.02), loc='upper left')  # needs seaborn 0.11.2

    ax.axhline(y=1, linewidth=2, color='black')
    plt.text(x=len(BENCHMARKS.keys()) - 0.5, y=1, s='Correct\ne. time\navg.', fontsize=20)
    # ax.annotate('Timeout', xy=(1.02, 0.79), xytext=(1.1, 0.79), xycoords='axes fraction',
    #             ha='center', va='center',
    #             arrowprops=dict(arrowstyle='-[, widthB=7.5, lengthB=1.', lw=2.0), annotation_clip=False)
    ax.set_ylabel("Erroneous vs correct average execution time ratio", fontsize=19)
    ax.set_xlabel("")
    ax.set_ylim([0, 3])
    ax.tick_params(labelrotation=0, labelsize=20)
    ax.legend(bbox_to_anchor=(0, 1.01, 0.95, 0.2), loc="lower left", mode="expand", borderaxespad=0, ncol=4,
              frameon=False,
              fontsize=20, edgecolor="black", markerscale=1.8, columnspacing=0.2, handletextpad=0.09)
    # Modify the point edge colour
    for ha in ax.legend_.legendHandles:
        ha.set_edgecolor("black")

    plt.tight_layout()
    # plt.savefig("/home/fernando/git_research/iolts_riscvw_2022/fig/execution_time_diff.pdf")
    plt.savefig("/home/fernando/git_research/iolts_riscvw_2022/fig/execution_time_diff.svg")

    plt.show()
    # draw_exec_time_comparison(df=df)
    return df_no_timeout


def plot_exec_time_distribution_boxplot(df):
    # df["ratio_exec_time"] = df["iteration_time"] / df["no_error_execution_time"]
    # # Remove the outliers
    # df = df[~((df["benchmark"] == "Fir") & (df["ratio_exec_time"] > 9))]
    # df = df[~((df["benchmark"] == "MatMult") & (df["ratio_exec_time"] > 8.2))]
    # df = df[~((df["benchmark"] == "Mnist") & (df["ratio_exec_time"] > 10))]
    df = df[df["benchmark"] != "MnistGraph"]
    df = df[df["outcome"] != "Not an error"]
    df["benchmark"] = df["benchmark"].apply(lambda x: BENCHMARKS[x])
    # Remove incorrect config for bi linear
    df = df.drop(
        df[(df["benchmark"] == "BiLinear Resize") & (df["outcome"] == "Timeout") & (df["iteration_time"] < 50)].index)
    # Get the colors and markers for the legend
    time_outcome = df[["benchmark", "outcome", "no_error_execution_time", "iteration_time"]]
    time_outcome.groupby(["benchmark", "outcome"]).describe().to_csv("/tmp/test.csv")
    df.loc[df["outcome"] == "Runtime error", "outcome"] = "SDK exception"

    df = df.set_index("benchmark")
    df = df.loc[list(BENCHMARKS.values())].reset_index()

    df_no_timeout = df[df["outcome"] != "Timeout"]
    ###########################################################################################################
    # EXEC TIME ONLY
    print(df_no_timeout[["benchmark", "iteration_time", "time"]].groupby("benchmark").describe())
    ax = sns.boxplot(x="benchmark", y="iteration_time", data=df_no_timeout, hue="outcome")
    ax.set_ylim([0, 13])
    ax.legend(bbox_to_anchor=(0, 1.01, 1, 0.2), loc="lower left", mode="expand", ncol=4, frameon=False,
              fontsize=8.5, edgecolor="black", markerscale=1.8, columnspacing=0.2, handletextpad=0.)
    plt.tight_layout()
    plt.show()


def draw_exec_time_comparison(df):
    exec_time_df = pd.read_csv(EXEC_TIME_CSV)
    all_colors = {k: v["color"] for k, v in MARKER_OPTIONS.items()}
    sns.boxplot(x="benchmark", y="ratio_exec_time", data=df, palette=all_colors, hue="outcome")
    plt.tight_layout()
    plt.savefig("/home/fernando/git_research/iolts_riscvw_2022/fig/incorrect_boxplot.pdf")
    plt.show()
    ax = sns.boxplot(x="benchmark", y="execution_time", data=exec_time_df)
    ax.set_ylim([0, math.ceil(exec_time_df["execution_time"].max())])

    print(exec_time_df.groupby("benchmark").describe())

    plt.tight_layout()
    plt.savefig("/home/fernando/git_research/iolts_riscvw_2022/fig/exec_time_boxplot.pdf")
    plt.show()


def select_outcome(row):
    if row["CRASH_ILLEGAL_INST"] == 1:
        return "Illegal instruction"
    if row["timeout"] == 1:
        return "Timeout"
    if row["SDC"] == 1:
        return EXEC_SDC
    if row["unicode"] == 1:
        return "SDK exception"
    if row["CRASH_MEM"] == 1:
        return "Memory error"
    if row["CRASH_RUNNER"] == 1:
        return "Runtime error"
    return "Not an error"


# TODO: Plot the values not the ratio on the erroneous execution time
# TODO: and add the outcomes

def plot_outcome_distribution(df, plot_mnist_graph=False):
    outcome_graph_path = "/home/fernando/git_research/iolts_riscvw_2022/fig/outcome_pct.pdf"
    if plot_mnist_graph:
        outcome_graph_path = "/home/fernando/git_research/iolts_riscvw_2022/fig/outcome_pct.svg"
        # BENCHMARKS['MnistGraph'] = "MNIST (Graph)"
    new_df = df.fillna(0)
    # print(new_df[new_df["outcome"] == "Not an error"])
    only_errors = new_df[new_df["outcome"] != "Not an error"].copy()
    only_errors["counter"] = 1
    counter = only_errors.groupby(["benchmark", "outcome"]).sum()
    values_sum = counter["counter"].groupby(level=0).sum()
    percentage = counter["counter"] / values_sum
    percentage = percentage.unstack().fillna(0)
    # Setting order
    categories = [EXEC_SDC, "Timeout", "Illegal instruction", "Memory error", "Runtime error",
                  "SDK exception"]
    colors = {k: v["color"] for k, v in MARKER_OPTIONS.items()}
    percentage = percentage[categories]
    percentage["SDK exception"] += percentage["Runtime error"]
    df_draw = percentage.drop("Runtime error", axis='columns').reindex(index=list(BENCHMARKS.values()))
    fig, ax = plt.subplots(figsize=(6.5, 5))
    df_draw.plot.bar(stacked=True, edgecolor="black", color=colors, ax=ax)
    # ax.set_xticklabels(BENCHMARKS.values())
    ax.set_xlabel("")
    ax.set_ylim([0, 1])
    ax.set_ylabel("% of outcomes observed in the experiments", fontsize=12)
    ax.tick_params(labelrotation=0)

    ax.legend(bbox_to_anchor=(0, 1.01, 1.02, 0.2), loc="lower left", markerscale=1.8,
              mode="expand", borderaxespad=0, ncol=5, frameon=False, fontsize=10, columnspacing=0.2, handletextpad=0.09)
    plt.tight_layout()
    # plt.subplots_adjust(top=0.9)

    plt.savefig(outcome_graph_path)

    plt.show()
    return df_draw


def parse_mnist_critical_errors(df):
    df = df[df["benchmark"] == BENCHMARKS["Mnist"]]
    # From grep "Recognized number" *Mnist_* -r  | grep -v "Recognized number : 6"  | wc -l
    critical_sdc_count = 15
    not_critical_sdc_count = df[df["CRITICAL_SDC"] != 1].shape[0]
    print(df[df["CRITICAL_SDC"] != 1])
    print("critical errors %", critical_sdc_count / not_critical_sdc_count * 100)
    print("tolerable errors %", (1 - critical_sdc_count / not_critical_sdc_count) * 100)


def main():
    csv_path = "../data/parsed_ChipIR092022/parsed_data.csv"
    df = pd.read_csv(csv_path, sep=";")
    df = df[df["benchmark"] != "MnistGraph"]
    df["outcome"] = df.apply(select_outcome, axis="columns")
    df["benchmark"] = df["benchmark"].apply(lambda x: BENCHMARKS[x])
    outcome_dist = plot_outcome_distribution(df=df)
    exec_dist = plot_exec_time_distribution(df=df)
    print(outcome_dist)
    print(exec_dist)
    parse_mnist_critical_errors(df=df)
    # plot_exec_time_distribution_boxplot(df=df)
    with  pd.ExcelWriter("/home/fernando/git_research/iolts_riscvw_2022/data/outcome.xlsx") as writer:
        outcome_dist.to_excel(writer, sheet_name="OutcomeDistribution")
        exec_dist.to_excel(writer, sheet_name="ExecutionDistribution")


if __name__ == '__main__':
    main()
