#!/usr/bin/env python3
import copy
import glob
import os
import re
from datetime import datetime
from typing import List

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from common import LOGS_DIR_092022, CNN_OPS_TO_TEST, GEOMETRY_PARSED_DATA_PATH_092022
from prepare_data_ChipIR092022 import find_standard_errors, parse_errors, extract_masked_data, filter_bad_errors
from select_runs_ChipIR092022 import get_cross_section_runs_09_2022

MASKED, SINGLE, LINE, SQUARE, RANDOM = "MASKED", "SINGLE", "LINE", "SQUARE", "RANDOM"


def has_grouped_ones(arr: np.ndarray) -> bool:
    for i in range(1, len(arr) - 1):
        if arr[i] == 1 and (arr[i - 1] == 1 or arr[i + 1] == 1):
            return True
    return False


def geometry_comparison(diff):
    error_format = MASKED
    count_non_zero_diff = np.count_nonzero(diff)
    num_dimensions = diff.ndim

    if count_non_zero_diff == 1:
        error_format = SINGLE
    elif count_non_zero_diff > 1:
        if num_dimensions == 1:
            if has_grouped_ones(arr=diff):
                error_format = LINE
            else:
                error_format = RANDOM
        else:
            # Use label function to labeling the matrix
            where_is_corrupted = np.argwhere(diff != 0)
            # Get all positions of X and Y
            all_x_positions = where_is_corrupted[:, 0]
            all_y_positions = where_is_corrupted[:, 1]
            # Count how many times each value is in the list
            unique_elements, counter_x_positions = np.unique(all_x_positions, return_counts=True)
            unique_elements, counter_y_positions = np.unique(all_y_positions, return_counts=True)

            # Check if any value is in the list more than one time
            row_error = np.any(counter_x_positions > 1)
            col_error = np.any(counter_y_positions > 1)

            if row_error and col_error:  # square error
                error_format = SQUARE
            elif row_error or col_error:  # row/col error
                error_format = LINE
            else:  # random error
                error_format = RANDOM
    return error_format


def parse_errors_cnn(data_list: list, cnn_op: str) -> [List[dict], np.ndarray, np.ndarray]:
    error_list = list()
    sizes = dict(MAX_POOL=(int(112 / 2), int(112 / 2)), CONV=(100, 100), LINEAR=(16,))
    cnn_op_size = sizes[
        cnn_op.
        replace("RAD_SEQUENTIAL_", "").
        replace("AVG_", "").
        replace("RAD_PARALLEL_VECT_", "")
    ]
    operation_heat_map = np.zeros(cnn_op_size)
    diff_mask = np.zeros(8)  # 8 is because of byte size
    for iteration_i_data in data_list:
        new_it_data = copy.deepcopy(iteration_i_data)
        new_it_data["benchmark"] = f"cnnop_{cnn_op}"
        err_list = new_it_data["err_list"]

        if err_list:
            # The case where the runner died for real
            if any(["Error: no device found" in err_list_it for err_list_it in err_list]):
                raise ValueError(f"RUN DIED FOR REAL, INVESTIGATE\n{err_list}")
            stdout_diff, stderr_diff, due, due_type = find_standard_errors(log_lines=err_list)
            new_it_data["SDC"] = 0
            new_it_data["stdout"] = stdout_diff
            new_it_data["stderr"] = stderr_diff
            new_it_data["DUE"] = due
            new_it_data["DUE_type"] = due_type
            diff_matrix = np.zeros(cnn_op_size)

            for line in err_list:
                # Search for SDC
                error_match = re.match(r"Error:\[(\d+)(?:,(\d+))?]=(-?\d+) != (-?\d+)", line.strip())
                if stdout_diff == 1 and error_match:
                    # Gold can be equal too, then not actually an error
                    i, j = int(error_match.group(1)), int(error_match.group(2)) if error_match.group(2) else None
                    found, gold = int(error_match.group(3)), int(error_match.group(4))

                    if found != gold:
                        diff_int = found ^ gold
                        # diff_int = abs(diff_int)
                        diff_bin = format(diff_int, '08b')
                        if '-' in diff_bin:
                            if len(diff_bin) == 9:
                                diff_bin = diff_bin.replace('-', '')
                            elif len(diff_bin) == 8:
                                diff_bin = diff_bin.replace('-', '0')
                            else:
                                print(diff_bin, len(diff_bin))
                                exit(0)
                        diff = np.array(list(map(int, diff_bin)))
                        diff_mask += diff

                        new_it_data["SDC"] = 1
                        if j is None:
                            diff_matrix[i] = 1
                        elif "LINEAR" not in cnn_op:
                            diff_matrix[i, j] = 1
                        else:
                            raise ValueError(f"Not valid size or operation:{line} {cnn_op_size} {i} {j}")
                    new_it_data["DUE"] = 0

                additional_info_m = re.match(r".*RATIT:(\S+) ITS:(\S+) TIME_IT:(\S+) CYCLE[_\\x7f]+IT:(\S+) .*", line)
                if additional_info_m:
                    try:
                        new_it_data["its"] = int(additional_info_m.group(2))
                    except ValueError:
                        new_it_data["its"] = None
                    try:
                        new_it_data["iteration_cycle"] = int(additional_info_m.group(4))
                    except ValueError:
                        new_it_data["iteration_cycle"] = None
            del new_it_data["err_list"]
            new_it_data["error_format"] = geometry_comparison(diff=diff_matrix)
            operation_heat_map += diff_matrix
            error_list.append(new_it_data)
    return error_list, operation_heat_map, diff_mask


def check_if_valid_error(start_dt, df_cs):
    interval_df = df_cs[(df_cs["start_dt"] <= start_dt) & (df_cs["end_dt"] >= start_dt)]
    return int(interval_df.shape[0] != 0)


def plot_heatmap(operations_heat_map: dict):
    for op, heat_map in operations_heat_map.items():
        if heat_map.ndim == 1:
            if heat_map.shape[0] == 16:
                heat_map = np.reshape(heat_map, (16, 1))
        gs = sns.heatmap(heat_map, cmap=sns.cm.rocket_r)
        gs.set_title(op)
        plt.tight_layout()
        plt.savefig(f"/home/fernando/git_research/iolts_2024/fig/rad_heatmaps/{op}_rad.jpg")
        plt.close()
        # plt.show()


def plot_bit_mask(operations_mask):
    df = pd.DataFrame(operations_mask)
    unstacked = df.unstack().reset_index()
    sns.catplot(unstacked, x="level_1", y=0, col="level_0", kind="bar", sharey=False)
    plt.savefig(f"/home/fernando/git_research/iolts_2024/fig/rad_bit_mask.jpg")

    plt.show()


def main():
    # First, get what is the valid runs from this experiment
    _, cross_section_df = get_cross_section_runs_09_2022()
    cross_section_df["start_dt"] = pd.to_datetime(cross_section_df["start_dt"])
    cross_section_df["end_dt"] = pd.to_datetime(cross_section_df["end_dt"])

    not_masked_data = list()
    masked_data = list()
    all_operations_heatmap = dict()
    all_operations_mask = dict()
    for log_file_path in glob.glob(f"{LOGS_DIR_092022}/*.log"):
        log_m = re.match(r"(\d+)_(\d+)_(\d+)_(\d+)_(\d+)_(\d+)_GA8-(\S+)_gap8.log", os.path.basename(log_file_path))
        (year, month, day, hour, minute, seconds, benchmark) = log_m.groups()
        start_log = datetime(year=int(year), month=int(month), day=int(day), hour=int(hour), minute=int(minute),
                             second=int(seconds))

        if check_if_valid_error(start_dt=start_log, df_cs=cross_section_df) is False:
            continue

        with open(log_file_path) as fp:
            log_lines = fp.readlines()
        header = log_lines[0]
        try:
            cnn_op = re.match(r".*cnnop:(\S+).*", header).group(1)
            assert cnn_op in CNN_OPS_TO_TEST
        except (AttributeError, AssertionError):
            cnn_op = None

        disable_check_seq_err_m = re.match(r".*disablecheckseqerr:(\S+) .*", header)
        disable_check_seq_err = bool(disable_check_seq_err_m.group(1)) if disable_check_seq_err_m else False

        err_list = parse_errors(data_list=log_lines, log_file_path=os.path.basename(log_file_path))
        masked_data.extend(
            extract_masked_data(data_list=err_list, mem_to_test="", cnn_op=cnn_op, benchmark=benchmark)
        )

        err_list = filter_bad_errors(log_errors=err_list)

        if cnn_op:
            if disable_check_seq_err is False:
                continue
            print(f"Parsing CNN OP:{cnn_op} {len(err_list)}")
            parsed_data, op_heatmap, diff_mask = parse_errors_cnn(data_list=err_list, cnn_op=cnn_op)
            if cnn_op not in all_operations_heatmap:
                all_operations_heatmap[cnn_op] = op_heatmap
                all_operations_mask[cnn_op] = diff_mask
            else:
                all_operations_heatmap[cnn_op] += op_heatmap
                all_operations_mask[cnn_op] += diff_mask

            not_masked_data.extend(parsed_data)

    plot_heatmap(operations_heat_map=all_operations_heatmap)
    plot_bit_mask(operations_mask=all_operations_mask)
    df = pd.DataFrame(not_masked_data)
    df.to_csv(GEOMETRY_PARSED_DATA_PATH_092022, index=False)


if __name__ == '__main__':
    main()
