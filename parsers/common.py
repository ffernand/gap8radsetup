import os
import pathlib
from datetime import datetime

import numpy as np
import pandas as pd

DEFAULT_REPO_DIR = os.path.join(pathlib.Path(__file__).parent.parent.absolute())

assert os.path.isdir(DEFAULT_REPO_DIR), f"Invalid repo dir: {DEFAULT_REPO_DIR}"

DATA_DIR = f"{DEFAULT_REPO_DIR}/data/past_experiments"
# LOGS_DIR = f"{DATA_DIR}/logs"
LOGS_DIR_032022 = f"{DATA_DIR}/logs_ChipIR032022"
LOGS_DIR_092022 = f"{DATA_DIR}/logs_ChipIR092022"
LOGS_DIR_122022 = f"{DATA_DIR}/logs_ChipIR122022"
LOGS_DIR_052022 = f"{DATA_DIR}/logs_ChipIR052024"

CHIPIR_032022_NEUTRON_COUNT_FILE = f"{LOGS_DIR_032022}/countlog/full_neutron_logs.txt"
CHIPIR_092022_NEUTRON_COUNT_FILE = f"{LOGS_DIR_092022}/countlog/full_neutron_logs.txt"
CHIPIR_122022_NEUTRON_COUNT_FILE = f"{LOGS_DIR_122022}/countlog/full_neutron_logs.txt"
CHIPIR_052024_NEUTRON_COUNT_FILE = f"{LOGS_DIR_052022}/countlog/full_neutron_logs.txt"

PARSED_DATA_032022 = f"{DATA_DIR}/parsed_ChipIR032022"
PARSED_DATA_092022= f"{DATA_DIR}/parsed_ChipIR092022"
PARSED_DATA_122022= f"{DATA_DIR}/parsed_ChipIR122022"
PARSED_DATA_052024= f"{DATA_DIR}/parsed_ChipIR052024"

PARSED_DATA_PATH_032022 = f"{PARSED_DATA_032022}/parsed_data.csv"
PARSED_DATA_PATH_092022 = f"{PARSED_DATA_092022}/parsed_data.csv"
PARSED_DATA_PATH_122022 = f"{PARSED_DATA_122022}/parsed_data.csv"
PARSED_DATA_PATH_052024 = f"{PARSED_DATA_052024}/parsed_data.csv"

# Only valid data cross validated with the cross-section parsers are in this files
PARSED_DATA_PATH_VALID_092022 = PARSED_DATA_PATH_092022.replace(".csv", "_valid.csv")

PARSED_DATA_MASKED_PATH_092022 = PARSED_DATA_PATH_092022.replace(".csv", "_masked.csv")
PARSED_DATA_MASKED_PATH_122022 = PARSED_DATA_PATH_122022.replace(".csv", "_masked.csv")

CSV_CROSS_SECTION_PATH_032022 = f"{PARSED_DATA_032022}/parsed_data_cross_section.csv"
CSV_CROSS_SECTION_PATH_092022 = f"{PARSED_DATA_092022}/parsed_data_cross_section.csv"
CSV_CROSS_SECTION_PATH_122022 = f"{PARSED_DATA_122022}/parsed_data_cross_section.csv"

# Error geometry for CNN operations
GEOMETRY_PARSED_DATA_PATH_092022 = f"{DATA_DIR}/parsed_ChipIR092022/geometry_parsed_data.csv"

EXEC_TIME_CSV_032022 = f"{PARSED_DATA_032022}/exec_time_csv.csv"

DEFAULT_SEA_FLUX = 13e9

BENCHMARKS = {
    'Fir': "FIR",
    'MatrixAdd': "Matrix Add",
    'BilinearResize': "Bilinear Resize",
    'MatMult': "Matrix Mul",
    'Mnist': "MNIST",
    # 'MnistGraph': "MNIST (Graph)"
}

MARKER_OPTIONS = {
    "SDC": {'marker': '<', 'color': 'rebeccapurple',
            'rgba': (0.4, 0.2, 0.6, 1.0)},
    "DUE": {'marker': 'P', 'color': 'lightslategray',
            'rgba': (0.4666666666666667, 0.5333333333333333, 0.6, 1.0)},

    "Illegal instruction": {'marker': 'v', 'color': '#DBB40C',
                            'rgba': (0.8588235294117647, 0.7058823529411765, 0.047058823529411764, 1.0)},
    "Timeout": {'marker': '^', 'color': 'olivedrab',
                'rgba': (0.4196078431372549, 0.5568627450980392, 0.13725490196078433, 1.0)},
    "SDK exception": {'marker': 's', 'color': 'blue',
                      'rgba': (0.0, 0.0, 1.0, 1.0)},
    "Memory error": {'marker': '>', 'color': 'aqua',
                     'rgba': (0.0, 1.0, 1.0, 1.0)},

    # 'FTZ-ON': {'marker': '*', 'color': 'black',
    #            'rgba': (0.0, 0.0, 0.0, 1.0)},
    # 'PrecDiv-OFF': {'marker': 'X', 'color': 'darkorange',
    #                 'rgba': (1.0, 0.5490196078431373, 0.0, 1.0)},
    #
    # 'PrecSqrt-OFF': {'marker': '8', 'color': 'darkviolet',
    #                  'rgba': (1.0, 0.5490196078431373, 0.0, 1.0)}
}

EXEC_SDC = "SDC"
MARKER_OPTIONS[EXEC_SDC] = MARKER_OPTIONS["SDC"]


#
# MARKER_OPTIONS = {
#     "Fir": {'marker': 'v', 'color': '#DBB40C',
#             'rgba': (0.8588235294117647, 0.7058823529411765, 0.047058823529411764, 1.0)},
#     "MatrixAdd": {'marker': '^', 'color': 'olivedrab',
#                   'rgba': (0.4196078431372549, 0.5568627450980392, 0.13725490196078433, 1.0)},
#     'BilinearResize': {'marker': '<', 'color': 'rebeccapurple',
#                        'rgba': (0.4, 0.2, 0.6, 1.0)},
#     'MatMult': {'marker': 's', 'color': 'blue',
#                 'rgba': (0.0, 0.0, 1.0, 1.0)},
#     'Mnist': {'marker': '>', 'color': 'aqua',
#               'rgba': (0.0, 1.0, 1.0, 1.0)},
#     'MnistGraph': {'marker': 'P', 'color': 'lightslategray',
#                    'rgba': (0.4666666666666667, 0.5333333333333333, 0.6, 1.0)},
#     # 'FTZ-ON': {'marker': '*', 'color': 'black',
#     #            'rgba': (0.0, 0.0, 0.0, 1.0)},
#     # 'PrecDiv-OFF': {'marker': 'X', 'color': 'darkorange',
#     #                 'rgba': (1.0, 0.5490196078431373, 0.0, 1.0)},
#     #
#     # 'PrecSqrt-OFF': {'marker': '8', 'color': 'darkviolet',
#     #                  'rgba': (1.0, 0.5490196078431373, 0.0, 1.0)}
# }
def read_count_file(in_file_name: str):
    """
    Read neutron log file
    :param in_file_name: neutron log filename
    :return: numpy array with all neutron lines
    """
    file_lines = list()
    with open(in_file_name, 'r') as in_file:
        for line in in_file:
            # Sanity check, we require a date at the beginning of the line
            line_split = line.rstrip().split()
            if len(line_split) < 7:
                print(f"Ignoring line (malformed):{line}")
                continue
            year_date, day_time, sec_frac = line_split[0], line_split[1], line_split[2]
            fission_counter = float(line_split[6])

            # Generate datetime for line
            cur_dt = datetime.strptime(year_date + " " + day_time + sec_frac, "%d/%m/%Y %H:%M:%S.%f")
            # It is faster to modify the lines on source
            file_lines.append(np.array([cur_dt, fission_counter]))
    return np.array(file_lines)


def get_fluency_flux(start_dt: datetime, end_dt, neutron_count: np.array, facility_factor: float,
                     distance_attenuation: float):
    """
    -- Fission counters are the ChipIR counters -- index 6 in the ChipIR log
    -- Current Integral are the synchrotron output -- index 7 in the ChipIR log
    """
    # Three seconds as ChipIR logs have this interval
    three_seconds = pd.Timedelta(seconds=3)
    # Slicing the neutron count to use only the useful information
    # It is efficient because it returns a view of neutron count
    neutron_count_cut = neutron_count[(neutron_count[:, 0] >= start_dt) &
                                      (neutron_count[:, 0] <= (end_dt + three_seconds))]
    beam_off_time, last_fission_counter = 0, None
    # Get the first from the list
    last_dt, first_fission_counter = neutron_count_cut[0]
    # Loop thought the neutron to find the beam off
    for (cur_dt, fission_counter) in neutron_count_cut[1:]:
        if fission_counter == last_fission_counter:
            beam_off_time += (cur_dt - last_dt).total_seconds()
        last_fission_counter = fission_counter
        last_dt = cur_dt

    interval_total_seconds = float((end_dt - start_dt).total_seconds())
    flux = ((last_fission_counter - first_fission_counter) * facility_factor) / interval_total_seconds
    error_str = f"FLUX<0 {start_dt} {end_dt} {flux} {last_fission_counter} {interval_total_seconds} {beam_off_time}"
    assert flux >= 0, error_str

    flux *= distance_attenuation
    return flux, beam_off_time


def get_end_times(group):
    # Check if it is sorted
    assert all([i == j for i, j in zip(group.sort_values(), group)])
    # print(group == group.sort_values())
    # print(group.equals(group.sort_values()))
    # assert group.equals(group.sort_values())
    end_dt_group = group.copy()
    # Find the time slots
    first = group.iloc[0]
    time_slot_for_fluency = pd.Timedelta(seconds=SECONDS_1h)
    for i in range(len(group)):
        if (group.iloc[i] - first) > time_slot_for_fluency:
            first = group.iloc[i]
        end_dt_group.iloc[i] = first + time_slot_for_fluency
    return end_dt_group


def generate_cross_section(row, neutron_count, factor, distance_attenuation):
    start_dt = row["start_dt"]
    end_dt = row["end_dt"]
    acc_time = row["acc_time"]
    sdc_s = row["SDC"]
    due_s = row["DUE"]
    # Conversion factor (as usual 4th column of counters to get n/cm2)
    print(f"Generating cross section for {row['benchmark']}, start {start_dt} end {end_dt}")
    flux, time_beam_off = get_fluency_flux(start_dt=start_dt, end_dt=end_dt, neutron_count=neutron_count,
                                           facility_factor=factor, distance_attenuation=distance_attenuation)
    fluency = flux * acc_time
    cross_section_sdc = cross_section_due = 0
    if fluency > 0:
        cross_section_sdc = sdc_s / fluency
        cross_section_due = due_s / fluency

    row["end_dt"] = end_dt
    row["Flux 1h"] = flux
    row["Fluency(Flux * $AccTime)"] = fluency
    row["Cross Section SDC"] = cross_section_sdc
    row["Cross Section DUE"] = cross_section_due
    row["Time Beam Off"] = time_beam_off
    return row


# Time for each run
SECONDS_1h = 3600
MEMORIES_TO_TEST = {
    "L1": "-DMEM_LEVEL=1",
    "L2": "-DMEM_LEVEL=2"
}

MEMORIES_NAMES = {
    "mem_L1": "L1", "mem_L2": "L2"
}

CNN_OPS_TO_TEST = dict(
    RAD_SEQUENTIAL_MAX_POOL=0,
    RAD_SEQUENTIAL_AVG_MAX_POOL=1,
    RAD_SEQUENTIAL_CONV=2,
    RAD_SEQUENTIAL_LINEAR=3,
    RAD_PARALLEL_VECT_MAX_POOL=14,
    RAD_PARALLEL_VECT_AVG_MAX_POOL=15,
    RAD_PARALLEL_VECT_CONV=16,
    RAD_PARALLEL_VECT_LINEAR=17,
)

CNN_MICRO_NAME = {
    "PARALLEL_VECT_AVG_MAX_POOL": "Avgpool",
    "PARALLEL_VECT_CONV": "Convolution", "PARALLEL_VECT_LINEAR": "Linear", "PARALLEL_VECT_MAX_POOL": "Maxpool",
    "SEQUENTIAL_AVG_MAX_POOL": "Avgpool", "SEQUENTIAL_CONV": "Convolution",
    "SEQUENTIAL_LINEAR": "Linear", "SEQUENTIAL_MAX_POOL": "Maxpool"
}
