#!/usr/bin/env python3

import os
import pickle

import numpy as np
from matplotlib import pyplot as plt

DATA_PATH = "/home/fernando/git_research/gap8radsetup/parsers/data"


def parse_memory_patterns(data: list) -> list:
    # As I'm injecting only 2 cell upset I have to filter it first

    if len(data) > 2:
        return []
    pos_diff_list = list()
    sorted_data = sorted(data, key=lambda x: x[0])
    for i in range(len(sorted_data) - 1):
        current_tuple = sorted_data[i]
        next_tuple = sorted_data[i + 1]
        pos_i, pos_i1 = (current_tuple[0], next_tuple[0])
        pos_diff_list.append(pos_i1 - pos_i)
    #
    # if len(data) == 1:
    #     pos_diff_list.append(0)

    return pos_diff_list


def main():
    mem_dict = {"L1": list(), "L2": list()}
    error_sizes = {"L1": list(), "L2": list()}

    for filename in os.listdir(DATA_PATH):
        f = os.path.join(DATA_PATH, filename)
        # checking if it is a file
        if os.path.isfile(f):
            print(f)
            with open(f, 'rb') as fp:
                data = pickle.load(fp)
            for entry in data:
                mem_type = entry["mem_type"]
                data = entry["data"]
                diff_list = parse_memory_patterns(data=data)
                mem_dict[mem_type] += diff_list
                error_sizes[mem_type].append(len(data))

    # TODO: the idea is simple: I create a function that generates the random bit injection based on the data (chat gpt)
    #   then I can also do the same for the instructions injection, where I have a function that selec
    fig, ax = plt.subplots(1, 2, sharey="none")
    for i, mem_type in enumerate(mem_dict):
        mem_array = np.array(mem_dict[mem_type])
        print("\n\n", mem_type, )
        # Find unique values and sort them
        unique_values = np.sort(np.unique(mem_array))

        # Generate the histogram with unique values as bin edges
        hist, bins = np.histogram(mem_array, bins=unique_values)
        hist_norm = hist / hist.sum()
        print("l1_hist_norm =", hist_norm.tolist())
        print("l2_bins =", bins.tolist())
        random_values = np.random.choice(bins[:-1], size=1000, p=hist_norm)
        # ax[i].hist(random_values, bins=bins[:-1])
        # ax[i].set_title(mem_type)
    # plt.yscale("log")
    # plt.show()


if __name__ == "__main__":
    main()
