#!/usr/bin/python3.8
import math

import pandas as pd

from common import CSV_CROSS_SECTION_PATH_092022, CNN_MICRO_NAME, BENCHMARKS, MEMORIES_NAMES


def calc_err_sdc(row):
    return (1.96 * row["Cross Section SDC"]) / (math.sqrt(row["SDC"]))


def calc_err_due(row):
    return (1.96 * row["Cross Section DUE"]) / (math.sqrt(row["DUE"]))


def get_cross_section_runs_09_2022():
    df = pd.read_csv(CSV_CROSS_SECTION_PATH_092022)
    good_runs_condition = (
        # only the ones that have ACC time
            (df["acc_time"] > 0) &
            # Remove broken runs
            # more than 1/3
            (df["Time Beam Off"] / df["acc_time"] <= 0.3)
    )
    # Select the bad runs
    bad_runs = df[~good_runs_condition]
    # Select the good runs
    df = df[good_runs_condition]
    df["VFS Prof."] = "opt"
    names = {**BENCHMARKS, **CNN_MICRO_NAME, **MEMORIES_NAMES}
    df["Micro"] = df["benchmark"].apply(lambda x: names[x])
    df["Exec type"] = df["benchmark"].apply(lambda x: "Parallel" if "PARALLEL" in x else "Sequential")
    return bad_runs, df


def main():
    bad_runs, df = get_cross_section_runs_09_2022()

    cross_section = df.groupby(["Micro", "Exec type", "VFS Prof."]).sum()

    """ Calc cross section """
    cross_section["Cross Section SDC"] = (cross_section["SDC"] / cross_section["Fluency(Flux * $AccTime)"])
    cross_section["Cross Section DUE"] = (cross_section["DUE"] / cross_section["Fluency(Flux * $AccTime)"])
    cross_section["Err SDC"] = cross_section.apply(calc_err_sdc, axis="columns")
    cross_section["Err DUE"] = cross_section.apply(calc_err_due, axis="columns")
    with pd.ExcelWriter("~/git_research/nsrec_2023/data/gap8_cross_section_sep2022_tmp.xlsx") as writer:
        bad_runs.to_excel(writer, sheet_name="BadRuns")
        df[df["benchmark"].str.contains("mem_")].to_excel(writer, sheet_name="Mem")
        df[df["benchmark"].str.contains("PARALLEL") |
           df["benchmark"].str.contains("SEQUENTIAL")].to_excel(writer, sheet_name="CNNOps")
        cross_section.to_excel(writer, sheet_name="CrossSection")


if __name__ == '__main__':
    main()
