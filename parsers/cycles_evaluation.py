#!/usr/bin/python3
import json
import os.path
import re

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from common import CSV_DIR, CNN_OPS_TO_TEST

PRETTY_NAMES = dict(
    cnnop_RAD_SEQUENTIAL_MAX_POOL="Seq M Pool",
    cnnop_RAD_PARALLEL_VECT_MAX_POOL="Par M Pool",
    cnnop_RAD_SEQUENTIAL_CONV="Seq Conv",
    cnnop_RAD_PARALLEL_VECT_CONV="Par Conv",
    cnnop_RAD_SEQUENTIAL_LINEAR="Seq Linear",
    cnnop_RAD_PARALLEL_VECT_LINEAR="Par Linear",
)


def load_golds():
    cycles_series = pd.Series(dtype=pd.Float64Dtype)
    for cnn_ops in CNN_OPS_TO_TEST:
        file_path = f"../data/cnn.json.{cnn_ops}"
        if os.path.isfile(file_path):
            with open(file_path) as fp:
                data = json.load(fp)
                cycles_series[f"cnnop_{cnn_ops}"] = float(re.match(r".*CYCLE_IT:(\d+) .*", data["cycle_line"]).group(1))
    return cycles_series


def main():
    df_codes = pd.read_csv(CSV_DIR.replace(".csv", "_cnn.csv"))
    # df_codes = df_codes[~df_codes["benchmark"].isin(["mem_L1", "mem_L2"])]
    # print(df_codes[df_codes["timeout"] == 1])
    cycles_df = df_codes[["benchmark", "SDC", "iteration_cycle"]].copy()
    cycles_df = cycles_df[~cycles_df["iteration_cycle"].isna()]

    print(df_codes[["benchmark", "iteration_time"]].groupby(["benchmark"]).describe())
    print(df_codes[["benchmark", "iteration_cycle"]].groupby(["benchmark"]).describe())

    gold_cycles = load_golds()
    for cnn_ops in CNN_OPS_TO_TEST:
        if "AVG" in cnn_ops:
            continue
        print(gold_cycles[f"cnnop_{cnn_ops}"])
        # print(cycles_df.loc[cycles_df["benchmark"] == f"cnnop_{cnn_ops}"])
        cycles_df.loc[cycles_df["benchmark"] == f"cnnop_{cnn_ops}", "iteration_cycle"] /= gold_cycles[
            f"cnnop_{cnn_ops}"]

    print(cycles_df[(cycles_df["benchmark"] == "cnnop_RAD_SEQUENTIAL_CONV") & (cycles_df["SDC"] == 0)])
    print(cycles_df[(cycles_df["benchmark"] == "cnnop_RAD_SEQUENTIAL_CONV") & (cycles_df["SDC"] == 0)].describe())
    cycles_df = cycles_df.reset_index()
    cycles_df["benchmark"] = cycles_df["benchmark"].apply(lambda x: PRETTY_NAMES[x])
    # print(cycles_df[cycles_df["iteration_cycle"] < 1].describe())
    sns.boxplot(data=cycles_df, x="benchmark", y="iteration_cycle", hue="SDC", order=list(PRETTY_NAMES.values()))
    plt.show()


if __name__ == '__main__':
    main()
