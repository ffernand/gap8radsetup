#!/usr/bin/env python3

import pandas as pd

from common import PARSED_DATA_PATH_092022, CSV_CROSS_SECTION_PATH_092022, CHIPIR_092022_NEUTRON_COUNT_FILE
from common import read_count_file, generate_cross_section, get_end_times


def main():
    factor = 1110000
    distance_attenuation = 0.9088967543
    print(f"Generating cross section for {PARSED_DATA_PATH_092022}")
    print(f"- {CHIPIR_092022_NEUTRON_COUNT_FILE} for neutrons")
    # -----------------------------------------------------------------------------------------------------------------
    # We need to read the neutron count files before calling get_fluency_flux
    neutron_count = read_count_file(CHIPIR_092022_NEUTRON_COUNT_FILE)
    # csv_out_file_summary = CSV_CROSS_SECTION_DIR_092022
    print(f"in: {PARSED_DATA_PATH_092022}")
    print(f"out: {CSV_CROSS_SECTION_PATH_092022}")
    # -----------------------------------------------------------------------------------------------------------------
    # Read the input csv file
    input_df = pd.read_csv(PARSED_DATA_PATH_092022)  # .drop("file_path", axis="columns")
    input_df["benchmark"] = input_df["benchmark"].str.replace("cnnop_RAD_", "")
    input_df = input_df[(input_df["SDC"] == 1) | (input_df["DUE"] == 1)]

    # Before continue we need to invert the logic of abort and end
    # input_df["DUE"] = input_df.apply(classify_due_gap8, axis="columns")
    # Convert time to datetime
    input_df["time"] = pd.to_datetime(input_df["time"])
    # Rename the column
    input_df = input_df.rename(columns={"time": "start_dt"}).sort_values(by=["start_dt"])

    # TO USE only 1h ACC TIME and bigger acc times will be placed in chunks
    runs = input_df.copy()
    runs['end_dt'] = runs.groupby(['benchmark'])['start_dt'].transform(get_end_times)
    # runs['acc_time_delta'] = (runs['end_dt'] - runs['start_dt']).apply(lambda x: x.total_seconds())
    runs["original_acc_time"] = runs["acc_time"]

    # This is the trick that calculates the REAL ACC TIME of the 1h chunk
    runs['acc_time'] = runs.groupby(['benchmark', 'end_dt'])["start_dt"].transform(
        lambda r: (r.max() - r.min()).total_seconds())
    runs.to_csv(CSV_CROSS_SECTION_PATH_092022.replace(".csv", "_intermediary.csv"), index=False)

    runs = runs.groupby(['benchmark', 'end_dt']).agg({
        'start_dt': 'first', 'SDC': 'sum', 'acc_time': 'max', 'original_acc_time': 'max', 'DUE': 'sum',
        'file_path': "first"
    }).reset_index()

    ####################################################################################################################
    # Apply generate_cross section function
    final_df = runs.apply(generate_cross_section, axis="columns", args=(neutron_count, factor, distance_attenuation))
    # Reorder before saving
    final_df = final_df[
        ['start_dt', 'end_dt', 'benchmark', 'SDC', 'DUE', 'acc_time', "original_acc_time", 'Time Beam Off', 'Flux 1h',
         'Fluency(Flux * $AccTime)', 'Cross Section SDC', 'Cross Section DUE', "file_path"]]
    print(final_df)
    print(CSV_CROSS_SECTION_PATH_092022)
    final_df.to_csv(CSV_CROSS_SECTION_PATH_092022, index=False, date_format="%Y-%m-%d %H:%M:%S")


#########################################################
#                    Main Thread                        #
#########################################################
if __name__ == '__main__':
    main()
