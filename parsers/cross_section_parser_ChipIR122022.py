#!/usr/bin/env python3

import pandas as pd

from common import CSV_CROSS_SECTION_PATH_122022, get_end_times
from common import read_count_file, generate_cross_section, PARSED_DATA_PATH_122022


def main():
    neutron_count_file = "../data/logs_ChipIR122022/countlogs/full_neutron_logs.txt"
    factor = 717000
    distance_attenuation = 0.9474868379
    print(f"Generating cross section for {PARSED_DATA_PATH_122022}")
    print(f"- {neutron_count_file} for neutrons")
    # -----------------------------------------------------------------------------------------------------------------
    # We need to read the neutron count files before calling get_fluency_flux
    neutron_count = read_count_file(neutron_count_file)
    # csv_out_file_summary = CSV_CROSS_SECTION_DIR_122022
    print(f"in: {CSV_CROSS_SECTION_PATH_122022}")
    print(f"out: {CSV_CROSS_SECTION_PATH_122022}")
    # -----------------------------------------------------------------------------------------------------------------
    # Read the input csv file
    input_df = pd.read_csv(PARSED_DATA_PATH_122022)  # .drop("file_path", axis="columns")
    input_df["benchmark"] = input_df.apply(lambda r: r["benchmark"].replace("cnnop_RAD_", r["vfs_profile"] + "-"),
                                           axis="columns")
    input_df = input_df[(input_df["SDC"] == 1) | (input_df["DUE"] == 1)]

    # There is a case where the runner inside the GAP8 died, then all executions were timeouts (about 20 errors).
    # Only for december 2022
    input_df = input_df[~((input_df["seq_errors"] > 10) & (input_df["DUE"] == 1))]

    # Before continue we need to invert the logic of abort and end
    # input_df["DUE"] = input_df.apply(classify_due_gap8, axis="columns")
    # Convert time to datetime
    input_df["time"] = pd.to_datetime(input_df["time"])
    # Rename the column
    input_df = input_df.rename(columns={"time": "start_dt"}).sort_values(by=["start_dt"])

    # TO USE only 1h ACC TIME and bigger acc times will be placed in chunks
    runs = input_df.copy()
    runs['end_dt'] = runs.groupby(['benchmark'])['start_dt'].transform(get_end_times)
    # runs['acc_time_delta'] = (runs['end_dt'] - runs['start_dt']).apply(lambda x: x.total_seconds())
    runs["original_acc_time"] = runs["acc_time"]

    # This is the trick that calculates the REAL ACC TIME of the 1h chunk
    test_unique_acc_times = len(runs.groupby(['benchmark', 'end_dt']).sum()["acc_time"].unique())
    runs['acc_time'] = runs.groupby(['benchmark', 'end_dt'])["acc_time"].transform(lambda r: r.max() - r.min())
    runs.to_csv(CSV_CROSS_SECTION_PATH_122022.replace(".csv", "_intermediary.csv"), index=False)
    assert test_unique_acc_times == len(runs["acc_time"].unique()), "Incorrect acc time calculation"

    runs = runs.groupby(['benchmark', 'end_dt']).agg({
        'start_dt': 'first', 'SDC': 'sum', 'acc_time': 'max', 'original_acc_time': 'max', 'DUE': 'sum',
        'file_path': "first"
    }).reset_index()

    ####################################################################################################################
    # Apply generate_cross section function
    final_df = runs.apply(generate_cross_section, axis="columns", args=(neutron_count, factor, distance_attenuation))
    # Reorder before saving
    final_df = final_df[
        ['start_dt', 'end_dt', 'benchmark', 'SDC', 'DUE', 'acc_time', "original_acc_time", 'Time Beam Off', 'Flux 1h',
         'Fluency(Flux * $AccTime)', 'Cross Section SDC', 'Cross Section DUE', "file_path"]]
    print(final_df)
    print(CSV_CROSS_SECTION_PATH_122022)
    final_df.to_csv(CSV_CROSS_SECTION_PATH_122022, index=False, date_format="%Y-%m-%d %H:%M:%S")


#########################################################
#                    Main Thread                        #
#########################################################
if __name__ == '__main__':
    main()
