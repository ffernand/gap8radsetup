
EXEC = ./run_experiment.py

ITERATIONS = 10
GENERATE = 0
BENCHMARK = MatMult

ifeq ($(REPROGRAM), 1)
REPROGRAM = --reprogram
endif

all: generate test


generate:
	$(EXEC) --generate $(REPROGRAM)

test:
	$(EXEC) --iterations $(ITERATIONS) --benchmark $(BENCHMARK)